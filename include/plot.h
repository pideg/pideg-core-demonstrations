/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#ifndef PLOT_H
#define PLOT_H

#include <stdint.h>

extern void plot(int size, float *data);

extern void plotWithTitle(int size, float *data, char *title);

extern void plot2WithTitle(int size, float *data1, float *data2, char *title);

#endif