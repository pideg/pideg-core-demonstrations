#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "constants.h"
#include "pideg.h"

typedef struct PIDEG_Simulator
{
    char *envelopeName;
    PIDEG pideg;
    int leaderCurveLength;
    float leaderCurve[SAMPLING_RATE * 1000];
} PIDEGSim;

extern void PIDEGSimInitialize(PIDEGSim *pidegsim, char *envelopeName);

extern void PIDEGSimCalculateLeaderCurve(PIDEGSim *pidegsim, int leaderCurveType, int leaderCurveLength);

extern void PIDEGSimSetIntegralWindup(PIDEGSim *pidegsim, int setIntegralWindup);

extern void PIDEGSimulateEnvelope(PIDEGSim *pidegsim);

#endif