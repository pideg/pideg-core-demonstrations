/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

//* Operational Constants
#define SAMPLING_RATE 1000
#define TAKEOVER_POINT 0.05
#define TAKEOVER_SAMPLES 250

//* PIDE Related Constants
#define TOTAL_PARAMETERS 5
#define PID_SAMPLE_SKIP 10

//* Predefined functions
#define MAX(a, b) ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define MIN(a, b) ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#endif