#ifndef ADSR_H
#define ADSR_H

const float envTimings[5] = {0, 0.15, 0.35, 0.5, 1};
const float envValues[5] = {0, 1, 0.8, 0.8, 0};

extern float ADSRGenerateEnvelopeSample(float relativeIndex);

#endif