/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#ifndef PIDEG_H
#define PIDEG_H

#include "constants.h"
typedef struct PIDEGEnvelope
{
    float parameters[TOTAL_PARAMETERS];

    float previousError;
    float integral;

    float setPoint;
    float measuredValue;

    int sampleCount;
    int takeoverPointSample;

    int pidegMode;
    float changeOverValue;

    int generateLeaderCurve;
    float *leaderCurve;

    int hasIntegralWinding;
} PIDEG;

extern void PIDEGInitialize(PIDEG *pideg);

extern void PIDEGProvideLeaderCurve(PIDEG *pideg, float *leaderCurve, int takeoverPointSample);

extern void PIDEGSetIntegralWindupMode(PIDEG *pideg, int setIntegralWindup);

extern void PIDEGKeyOn(PIDEG *pideg);

extern void PIDEGKeyOff(PIDEG *pideg);

extern float PIDEGGenerateSample(PIDEG *pideg);

extern float PIDEGGenerateSampleTakeoverSampled(PIDEG *pideg);

#endif