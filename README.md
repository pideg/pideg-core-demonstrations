# PID Envelope Generator Core Library

A simulator to demonstrate audio envelopes generated using the [The PID Envelope Generator (PIDEG) Core library](https://gitlab.com/pid-envelope/pideg-core).

> The PID Envelope Generator (PIDEG) Core is a C based library that can be used to integrate the functionality of the [PID Envelope Generator (PIDEG)](https://arxiv.org/abs/2106.13966) specification in other applications.

<p align="center">
  <img src="docs/demo_screenshot.png" />
</p>

This repo was developed to draft the manuscript of the aforementioned paper, and builds over the core PIDEG functionality to provide some additional features. These include:

1. Provision to vary nature of leader curves (LCs). Available options include inverse-exponential, trapezoidal and ADSR LCs.
2. Toggle for preferentially enabling integral windup phenomenon in the PID algorithm.
3. Facilities to enabling and specififying the *Takeover Mechanism*.

Ultimately, these features may be integrated into the core library upon throrough rounds of testing and validation.

## Installation

Linux:

1. Clone this repo to local, switch to repo

```sh
git clone https://gitlab.com/pid-envelope/pideg-core-demonstrations.git

cd pideg-core-demonstrations
```

2. Generate the binary by using the `Makefile`

```sh
make
```

## Usage

### Prerequisite

[gnuplot](http://www.gnuplot.info/) must be installed in the system and available via CLI. Install it using your favourite package manager.

### Evaluating the library

#### Linux

Running the binary

```sh
./bin/main
```

To execute any changes made to the source code, rebuild the binary using `Makefile` before running it.

## Contributing

1. [Report Bugs or Request Features](https://gitlab.com/pid-envelope/pideg-core-demonstrations/-/issues)
2. Fork it (<https://gitlab.com/yourname/yourproject/fork>)
3. Create your feature branch (`git checkout -b feature/fooBar`)
4. Commit your changes (`git commit -am 'Add some fooBar'`)
5. Push to the branch (`git push origin feature/fooBar`)
6. Create a new Merge/Pull Request

## License

Distributed under the Mozilla Public License Version 2.0 License. See `LICENSE` for more information.
