#include <stdio.h>
#include <math.h>
#include "simulator.h"
#include "constants.h"
#include "plot.h"
#include "adsr.h"

void PIDEGSimInitialize(PIDEGSim *pidegsim, char *envelopeName)
{
    pidegsim->envelopeName = envelopeName;
    PIDEGInitialize(&pidegsim->pideg);
}

void PIDEGSimCalculateLeaderCurve(PIDEGSim *pidegsim, int leaderCurveType, int leaderCurveLength)
{
    for (int i = 0; i <= leaderCurveLength / 2; i++)
    {
        switch (leaderCurveType)
        {
        case 1:
            //Trapezoidal LC
            pidegsim->leaderCurve[i] = MIN(1.0, pidegsim->pideg.parameters[3] * i / SAMPLING_RATE);
            break;
        case 2:
            //ADSR LC
            pidegsim->leaderCurve[i] = ADSRGenerateEnvelopeSample(1.0 * i / leaderCurveLength);
            break;
        default:
            // Inverse-Exponential LC
            pidegsim->leaderCurve[i] = 1 - exp(-pidegsim->pideg.parameters[3] * i / SAMPLING_RATE);
        }
    }

    for (int i = 0; i < leaderCurveLength / 2; i++)
    {
        switch (leaderCurveType)
        {
        case 1:
            //Linear LC
            pidegsim->leaderCurve[leaderCurveLength / 2 + i] = MAX(0, 1.0 - pidegsim->pideg.parameters[4] * i / SAMPLING_RATE);
            break;
        case 2:
            //ADSR LC
            pidegsim->leaderCurve[leaderCurveLength / 2 + i] = ADSRGenerateEnvelopeSample(0.5 + 1.0 * i / leaderCurveLength);
            break;
        default:
            // Inverse-Exponential LC
            pidegsim->leaderCurve[leaderCurveLength / 2 + i] = pidegsim->leaderCurve[leaderCurveLength / 2 - 1] * exp(-pidegsim->pideg.parameters[4] * i / SAMPLING_RATE);
        }
    }

    pidegsim->leaderCurveLength = leaderCurveLength;

    int takeoverPointSample;
    switch (leaderCurveType)
    {
    case 1:
        //Trapezoidal LC
    case 2:
        //ADSR LC
        takeoverPointSample = SAMPLING_RATE / pidegsim->pideg.parameters[4];
        break;
    default:
        // Inverse-Exponential LC
        takeoverPointSample = leaderCurveLength / 2 + 9.21 * SAMPLING_RATE / pidegsim->pideg.parameters[4];
    }

    PIDEGProvideLeaderCurve(&pidegsim->pideg, pidegsim->leaderCurve, takeoverPointSample);
}

void PIDEGSimSetIntegralWindup(PIDEGSim *pidegsim, int setIntegralWindup)
{
    PIDEGSetIntegralWindupMode(&pidegsim->pideg, setIntegralWindup);
}

void PIDEGSimulateEnvelope(PIDEGSim *pidegsim)
{
    float envelopeSamples[pidegsim->leaderCurveLength];

    PIDEGKeyOn(&pidegsim->pideg);
    for (int i = 0; i < pidegsim->leaderCurveLength / 2; i++)
    {
        envelopeSamples[i] = PIDEGGenerateSample(&pidegsim->pideg);
    }

    PIDEGKeyOff(&pidegsim->pideg);
    for (int i = pidegsim->leaderCurveLength / 2; i < pidegsim->leaderCurveLength; i++)
    {
        envelopeSamples[i] = PIDEGGenerateSample(&pidegsim->pideg);
    }

    printf("%s\n", pidegsim->envelopeName);
    for (int i = 0; i < pidegsim->leaderCurveLength; i++)
    {
        printf("%f,%f\n", pidegsim->leaderCurve[i], envelopeSamples[i]);
    }

    plotWithTitle(pidegsim->leaderCurveLength, envelopeSamples, pidegsim->envelopeName);
}
