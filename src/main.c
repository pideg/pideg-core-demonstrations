#include <stdio.h>
#include "constants.h"
#include "simulator.h"

int main(void)
{
    long totalSimulationSamples = SAMPLING_RATE * 10;

    PIDEGSim pidegsim;
    PIDEGSimInitialize(&pidegsim, "Simulated PID Envelope");
    PIDEGSimSetIntegralWindup(&pidegsim, 0);

    pidegsim.pideg.parameters[0] = 0.001;
    pidegsim.pideg.parameters[1] = 0.001;
    pidegsim.pideg.parameters[2] = 0.001;
    pidegsim.pideg.parameters[3] = 1;
    pidegsim.pideg.parameters[4] = 1;

    PIDEGSimCalculateLeaderCurve(&pidegsim, 0, totalSimulationSamples);

    PIDEGSimulateEnvelope(&pidegsim);

    return 0;
}