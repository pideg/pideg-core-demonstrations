#include "adsr.h"
#include "constants.h"

float ADSRGenerateEnvelopeSample(float relativeIndex)
{
    for (int i = 3; i >= 0; i--)
    {
        if (relativeIndex >= envTimings[i])
        {
            return envValues[i] + (envValues[i + 1] - envValues[i]) * (relativeIndex - envTimings[i]) / (envTimings[i + 1] - envTimings[i]);
        }
    }

    return 0;
}
