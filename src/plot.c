/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#include <stdio.h>
#include "plot.h"

void plot(int size, float *data)
{
    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    fprintf(gnuplot_pipe, "set title '%s'\n", "PID Envelope");
    fprintf(gnuplot_pipe, "plot '-'\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(gnuplot_pipe, "%d %f\n", i, data[i]);
    }

    fprintf(gnuplot_pipe, "e\n");

    fprintf(gnuplot_pipe, "refresh\n");
}

void plotWithTitle(int size, float *data, char *title)
{
    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    fprintf(gnuplot_pipe, "set title '%s'\n", title);
    fprintf(gnuplot_pipe, "plot '-'\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(gnuplot_pipe, "%d %f\n", i, data[i]);
    }

    fprintf(gnuplot_pipe, "e\n");

    fprintf(gnuplot_pipe, "refresh\n");
}

void plot2WithTitle(int size, float *data1, float *data2, char *title)
{
    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    fprintf(gnuplot_pipe, "set title '%s'\n", title);
    fprintf(gnuplot_pipe, "plot '-'\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(gnuplot_pipe, "%d %f %f\n", i, data1[i], data2[i]);
    }

    fprintf(gnuplot_pipe, "e\n");

    fprintf(gnuplot_pipe, "refresh\n");
}
